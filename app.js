const express = require('express')
const app = express()
const cors = require('cors')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

// requiring routes
const songsRoutes = require('./api/routes/songs')
const userRoutes = require('./api/routes/user')

app.use(morgan('dev'))

// parsing request body 
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// conecting to mongodb
mongoose.connect(
    `mongodb+srv://gilberto:${ process.env.MONGO_ATLAS_PW }@cluster0-qclz6.mongodb.net/syberiaquotes?retryWrites=true&w=majority`,{
    useNewUrlParser: true ,
    useCreateIndex: true,
})
mongoose.Promise = global.Promise

// fixing cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept, Authorization")
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE')
        return res.status(200).json({})
    }
    next()
})
app.use(cors())

// elementary routes
app.use('/songs', songsRoutes)
app.use('/user', userRoutes)

// errors handling
app.use((req, res, next) => {
    const error = new Error('Not found')
    error.status = 404
    next(error)
})

app.use((error, req, res, next) => {
    res.status(error.status || 500)
    res.json({
        error: {
            message: error.message
        }
    })
})

module.exports = app