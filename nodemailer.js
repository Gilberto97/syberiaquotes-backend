const nodemailer = require('nodemailer')

const smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    host: "smtp.gmail.com",
    port: 587,
    secure: true,
    auth: {
        user: "syberiaquotes@gmail.com",
        pass: "hcbddnlahwabfvhq",
    }
});

exports.sendVerifyEmail = (email, name, secretToken) => {
    
    const mailOptions = {
        from: '"SyberiaQuotes" <syberiaquotes@gmail.com',
        to: email,
        subject: 'Aktywacja Konta',
        text: `Witaj ${name}, Twój klucz aktywacyjny to: ${secretToken} . Aktywuj poprzez link: http://localhost:3000/user/verify/${secretToken}`
    }
    return smtpTransport.sendMail(mailOptions)
}

exports.sendResetPassword = (email,name, password) => {
    
    const mailOptions = {
        from: '"SyberiaQuotes" <syberiaquotes@gmail.com',
        to: email,
        subject: 'Reset Hasła',
        text: `Witaj ${name}, Twoje nowe hasło to: ${password}`
    }
    return smtpTransport.sendMail(mailOptions)
}