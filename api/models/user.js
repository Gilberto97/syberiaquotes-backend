const mongoose = require('mongoose')
const validator = require('validator')
const uniqueValidator = require('mongoose-unique-validator')
const moment = require('moment')
const randomString = require('randomstring')

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: {
        type: String,
        required: 'Please supply an email address',
        unique: true,
        lowercase: true,
        trim: true,
        validate: [validator.isEmail, 'Invalid Email Adress']
    },
    name: {
        type: String,
        required: 'Please supply a name',
        unique: true,
        lowercase: true,
        trim: true,
    },
    password: {
        type: String,
        required: 'Please supply a password',
    },
    isActivated: {
        type: Boolean,
        default: false,
    },
    secretToken: {
        type: String,
        default: randomString.generate(),
    },
    isVerifyEmailSent: {
        type: Boolean,
        default: false,
    },
    createdAt: {
        type: Date,
        default: moment().format(),
    },
    lastLoginAt: {
        type: Date,
        default: moment().format(),
    },
})

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema)