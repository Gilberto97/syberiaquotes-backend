const mongoose = require('mongoose')

const songSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {type: String, required: true},
    lyrics: {type: String, required: true},
    link: {type: String, required: true},
    addedBy: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User', 
        required: true
    },
})

module.exports = mongoose.model('Song', songSchema)