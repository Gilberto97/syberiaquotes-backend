const express = require('express')
const router = express.Router()

const UserController = require('../controllers/user')


router.post('/signup', UserController.create)

router.post('/login', UserController.login)

router.post('/verify/:secretToken', UserController.verify)

router.post('/sendVerifyEmail', UserController.sendVerifyEmailController)

router.post('/resetPassword', UserController.resetPassword)

router.post('/changePassword', UserController.changePassword)

router.delete('/delete', UserController.delete)

module.exports = router