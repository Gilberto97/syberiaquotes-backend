const express = require('express')
const router = express.Router()
const checkAuth = require('../middleware/check-auth')

const SongController = require('../controllers/songs')



//GET ALL SONGS
router.get('/', SongController.get_all)

// GET PARTICULAR SONG
router.get('/:songId', SongController.get_particular)

// CREATE NEW SONG
router.post('/', checkAuth, SongController.create)

// UPDATE PARTICULAR SONG
router.patch('/:songId', checkAuth, SongController.update)


module.exports = router