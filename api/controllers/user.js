const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const randomString = require('randomstring')
const passwordGenerator = require('generate-password')
const moment = require('moment')
const User = require('../models/user')
const { sendVerifyEmail, sendResetPassword } = require('../../nodemailer')

// CREATE NEW USER
exports.create = (req, res, next) => {
    const { email, name, password } = req.body

    User.find({$or:[{email:email},{name:name}]})
    .then(user => {
        if (user.length >= 1) {
            let message = ''
            user.forEach(u => {
                if (u.email == email) {
                    message = 'e-mail already exist!'
                } else if (u.name == name) {
                    message = 'name already exist!'
                }
            })
            return res.status(409).json({
                message: message
            })
        } else {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({
                        error: err
                    })
                } else {
                    const now = moment().format()
                    const user = new User({
                        _id: new mongoose.Types.ObjectId(),
                        email: email,
                        name: name,
                        password: hash,
                        isActivated: false,  
                        secretToken: randomString.generate(),
                        isVerifyEmailSent: false,
                        createdAt: now,
                        lastLoginAt: now,
                    })
                    user
                    .save()
                    .then(createdUser => {

                        const token = jwt.sign({
                                email: createdUser.email,
                                userId: createdUser._id
                            },
                            process.env.JWT_KEY,
                            {
                                expiresIn: '1h'
                            })
                        return res.status(200).json({
                            message: 'User created and Auth successful',
                            token: token,
                            userId: createdUser._id,
                            email: createdUser.email,
                            name: createdUser.name,
                            isActivated: createdUser.isActivated,
                        })
                    })
                    .catch(err => {
                        res.status(500).json({ message: err.message })
                    })
                }
            })
        }
    })
    .catch(err => {
        res.status(500).json({ error: err })
    })
}


// LOGIN USER
exports.login = (req, res, next) => {
    User.find({ email: req.body.email })
    .then(user => {
        if (user.length < 1) {
            return res.status(401).json({
                message: 'Auth failed'
            })
        }        
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            if (err) {
                return res.status(401).json({
                    message: 'Auth failed'
                })
            }
            if (result) {
                const now = moment().format()

                const token = jwt.sign({
                    email: user[0].email,
                    userId: user[0]._id
                },
                process.env.JWT_KEY,
                {
                    expiresIn: '1h'
                })
                
                user[0].lastLoginAt=now
                user[0]
                .updateOne(user[0])
                .catch(err => {
                    res.status(500).json({ error: err })
                })

                return res.status(200).json({
                    message: 'Auth successful',
                    token: token,
                    userId: user[0]._id,
                    email: user[0].email,
                    name: user[0].name,
                    isActivated: user[0].isActivated,
                })
            }
            return res.status(401).json({
                message: 'Auth failed'
            })
        })
    })
    .catch(err => {
        res.status(500).json({ error: err })
    })
}


// SEND VERIFICATION EMAIL
exports.sendVerifyEmailController = (req, res, next) => {

    User.find({ _id: req.body.id })
    .then(user => {
        if (user.length < 1) {
            return res.status(400).json({
                message: 'Wrong user ID'
            })
        } else if (user[0].isActivated === true) {
            return res.status(400).json({
                message: 'Email is already activated'
            })
        }

        const { email, name, secretToken } = user[0]
        console.log(secretToken)

        sendVerifyEmail(email, name, secretToken)
        .then(result => {

            user[0].isVerifyEmailSent=true;
            user[0]
            .updateOne(user[0])
            .catch(err => {
                res.status(500).json({ error: err })
            })

            return res.status(200).json({
                message: 'Activation email sent',
                result: result,
            })
        })
        .catch(err => {
            res.status(500).json({ error: err })
        })
    })
    .catch(err => {
        res.status(500).json({ error: err })
    })
}


// ACTIVATE USER EMAIL
exports.verify = (req, res, next) => {
    User.find({ secretToken: req.params.secretToken })
    .then(user => {
        if (user.length < 1) {
            return res.status(400).json({
                message: 'Wrong activation link'
            })
        } else if (user[0].isActivated === true) {
            return res.status(400).json({
                message: 'Email is already activated'
            })
        }
        
        user[0].isActivated = true
        user[0].secretToken = randomString.generate()
        user[0]
        .updateOne(user[0])
        .then(result => {
            console.log(result.data)
            return res.status(200).json({
                message: 'Activation successful',
            })
        })
        .catch(err => {
            res.status(500).json({ error: err })
        })

    })
    .catch(err => {
        res.status(500).json({ error: err })
    })
}


// RESET USER PASSWORD
exports.resetPassword = (req, res, next) => {
    User.find({ email: req.body.email })
    .then(user => {
        if (user.length < 1) {
            return res.status(401).json({
                message: 'Email not found'
            })
        }

        const { email, name } = user[0]

        const password = passwordGenerator.generate({
            length: 10,
            numbers: true
        })

        bcrypt.hash(password, 10, (err, hash) => {
            if (err) {
                return res.status(500).json({
                    error: err
                })
            } else {
                user[0].password = hash
                user[0]
                .updateOne(user[0])
                .then(() => {
                    sendResetPassword(email, name, password)
                    .then(result => {               
                        return res.status(200).json({
                            message: 'Reset password sent',
                            result: result,
                        })
                    })
                    .catch(err => {
                        res.status(500).json({ error: err })
                    })
                })
                .catch(err => {
                    res.status(500).json({ message: err.message })
                })
            }
        })
    })
    .catch(err => {
        res.status(500).json({ error: err })
    })
}

// CHANGE USER PASSWORD
exports.changePassword = (req, res, next) => {
    User.find({ email: req.body.email })
    .then(user => {
        if (user.length < 1) {
            return res.status(401).json({
                message: 'Auth failed'
            })
        }

        const { password, newPassword } = req.body
        console.log(password, newPassword)
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            console.log(result)
            if (err) {
                return res.status(401).json({
                    message: 'Error! Something goes wrong'
                })
            }
            if (result) {
                bcrypt.hash(newPassword, 10, (err, hash) => {
                    console.log(hash)
                    if (err) {
                        return res.status(500).json({
                            error: err
                        })
                    } else {
                        user[0].password = hash
                        user[0]
                        .updateOne(user[0])
                        .then(result => {
                            return res.status(200).json({
                                message: 'Password changed!',
                                result: result,
                            })
                        })
                        .catch(err => {
                            console.log("should work!")
                            res.status(500).json({ message: err.message })
                        })
                    }
                })
            } else {
                return res.status(401).json({
                    message: 'Auth failed!'
                })
            }
        })
    })
    .catch(err => {
        res.status(500).json({ error: err })
    })
}



exports.delete = (req, res, next) => {
    User.find({ email: req.body.email })
    .then(user => {
        if (user.length < 1) {
            return res.status(401).json({
                message: 'No user found'
            })
        }

        const { password, newPassword } = req.body
        console.log(password, newPassword)
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            if (err) {
                return res.status(401).json({
                    message: 'Error! Something goes wrong'
                })
            }
            if (result) {
                user[0]
                .deleteOne(user[0])
                .then(user => {
                    return res.status(200).json({
                        message: 'User deleted',
                        userId: user._id,
                        email: user.email,
                        name: user.name,
                    })
                })
            } else {
                return res.status(401).json({
                    message: 'Auth failed!'
                })
            }
        })
    })
    .catch(err => {
        res.status(500).json({ error: err })
    })
}