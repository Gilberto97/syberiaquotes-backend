const Song = require('../models/song')
const mongoose = require('mongoose')


// GET ALL SONGS
exports.get_all = (req, res, next) => {
    Song.find()
        .select('_id title lyrics link userId')
        // .select('addedBy')
        // .populate('addedBy')
        .then(docs => {
            const response = docs
            res.status(200).json(response)
        })
        .catch(err => {
            res.status(500).json({ error: err })
        })
}


// GET PARTICULAR SONG
exports.get_particular = (req, res, next) => {
    const id = req.params.songId
    Song.findById(id)
        .select('_id title lyrics link')
        // .select('addedBy')
        // .populate('addedBy')
        .then(doc => {
            if (doc) {
                res.status(200).json(doc)
            } else {
                res.status(400).json({ message: 'No valid entry found for provided ID' })
            }
        })
        .catch(err => {
            res.status(500).json({ error: err })
        })
}


// CREATE NEW SONG
exports.create = (req, res, next) => {
    console.log(req.userData)
    const song = new Song({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        lyrics: req.body.lyrics,
        link: req.body.link,
        addedBy: req.userData.userId,
    })
    song
        .save()
        .then(result => {
            console.log(result)
            res.status(201).json({
                message: 'handling POST request to /songs',
                createdSong: result
            })
        })
        .catch(err => {
            res.status(500).json({ error: err })
        })

}


// UPDATE PARTICULAR SONG
exports.update = (req, res, next) => {
    const id = req.params.songId
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }
    Song.updateOne({ _id: id }, { $set: updateOps })
        .then(result => {
            res.status(200).json({
                message: 'Product updated',
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/songs/' + id
                }
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}