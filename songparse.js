const fs = require('fs')
// const songs = require('./songs.json')

// let songs;

modifySong = (key, object) => {
    // console.log(Object.keys(object))
    return(
        {
            title: key,
            lyrics: object.lyrics,
            link: object.link
        }
    )
}

const songsJsonString = fs.readFileSync('./songs.json', 'utf8', (err, jsonString) => {
    if (err) {
        console.log("File read failed:", err)
        return
    }
    try {
        console.log(JSON.parse(jsonString))
        return JSON.parse(jsonString)       
    } catch(err) {
        console.log('Error parsing JSON string:', err)
    }
})

const songsJson = JSON.parse(songsJsonString)
const songKeys = Object.keys(songsJson)

let newSongs = []

songKeys.forEach(key => {
    newSongs.push(modifySong(key, songsJson[key]))
})

console.log(newSongs)

const jsonString = JSON.stringify(newSongs)
fs.writeFileSync('./newSongs.json', jsonString)